# IAllowIt WIP

## A small app to keep track of monthly budget

One can add a monthly value and then edit (subtract or add) expenses on a daily basis. Remaining allowance for the rest of the month will be updated and distributed over the remaining number of days. Currently the app only supports local storage for data persistence.

Tech stack: React Native, StyleSheet API, Jest

<img src='https://gitlab.com/hussariush/iallowit/uploads/48ae015f5b20a4bcf19a3a6fd42f4877/Screenshot_20210215-162333.png' alt='list view of days with allowances' width='250'/>
<img src='https://gitlab.com/hussariush/iallowit/uploads/d802a8e057d94651fef909b5ed13bed8/Screenshot_20210215-162347.png' alt='list view of days with allowances' width='250'/>
<img src='https://gitlab.com/hussariush/iallowit/uploads/7945a29dcf1f32e6a69a8b22fe945420/Screenshot_20210215-162411.png' alt='list view of days with allowances' width='250'/>
