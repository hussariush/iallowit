// allowance from today until end of month:
// split month into before today and from today
// sum allowances from beginning of month until today
// divide by days from today until end of month

import { DayAllowance } from "../components/Listing/Listing";

const dayOfMonth = new Date().getDate();
const test = [
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 1,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 2,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 3,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 4,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 5,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 6,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 7,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 8,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 9,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 10,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 11,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 12,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 13,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 14,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 15,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 16,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 17,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 18,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 19,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 20,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 21,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 22,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 23,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 24,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 25,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 26,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 27,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 28,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 29,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 30,
  },
  {
    additions: [],
    allowance: 10,
    deductions: [],
    id: 31,
  },
];
export interface UpdateObj {
  day: number,
  update: boolean
}

export type useAllowancesType = {
  DeductFromAllowance: (array: any[], itemIndex:number, deduction: string) => void;
  AddToAllowance: (array: any[], itemIndex:number, addition: string) => void;
  RecalculateAllowances: (
    data: DayAllowance[],
    updateObj?: UpdateObj
  ) => DayAllowance[];
};

const useAllowances = ():useAllowancesType => {
  const AddToAllowance = (
    array: any[],
    itemIndex: number,
    addition: string
  ) => {
    const oldAllowanceObj = array?.find((item, index) => index === itemIndex);
    const allowance =
      oldAllowanceObj && (oldAllowanceObj.allowance as number) + parseInt((addition), 10);
    const additions = oldAllowanceObj && [
      ...oldAllowanceObj.additions,
      addition,
    ];
    const newAllowanceObj = oldAllowanceObj && {
      ...oldAllowanceObj,
      allowance,
      additions,
    };
    const newAllowances = array!.map((item: any, index: number) => {
      if (itemIndex === index) {
        return newAllowanceObj;
      }
      return item;
    });
    return newAllowances;
  };
 
  const DeductFromAllowance = (
    array: any[],
    itemIndex: number,
    deduction: string
  ) => {
    const oldAllowanceObj = array?.find((item, index) => index === itemIndex);
    const allowance =
      oldAllowanceObj && (oldAllowanceObj.allowance as number) - parseInt(deduction, 10);
    const deductions = oldAllowanceObj && [
      ...oldAllowanceObj.deductions,
      deduction,
    ];
    const newAllowanceObj = oldAllowanceObj && {
      ...oldAllowanceObj,
      allowance,
      deductions,
    };
    const newAllowances = array!.map((item: any, index: number) => {
      if (itemIndex === index) {
        return newAllowanceObj;
      }
      return item;
    });
    return newAllowances;
  };

  const RecalculateAllowances = (
    data: DayAllowance[],
    { day = dayOfMonth, update = false } = {}
  ): DayAllowance[] => {
    // const dayInMonthPlusOne = new Date(new Date.getTime() + 86400000).getDate();
    // -1 to get dates before and excluding today
    const daysAfterToday = data.splice(day - 1);
    const daysBeforeToday = data.splice(0, day - 1);
    const sumOfPreviousAllowances = daysBeforeToday
    .map((item) => {
        return item.allowance || 0;
      })
      .reduce((acc, curr) => (acc as number) + (curr as number),0);
    const sumOfFutureAllowances = daysAfterToday
      .map((item) => {
        return item.allowance || 0;
      })
      .reduce((acc, curr) => (acc as number) + (curr as number),0);
    const updatedAllowancesFromToday = daysAfterToday.map((item) => {
      const allowance = !update
        ? ((sumOfPreviousAllowances as number) +
            (sumOfFutureAllowances as number)) /
          daysAfterToday.length
        : (sumOfFutureAllowances as number) / daysAfterToday.length;
      return { ...item, allowance };
    });
    const newBreakdown = [...daysBeforeToday, ...updatedAllowancesFromToday];
    return newBreakdown;
  };

  return {RecalculateAllowances, DeductFromAllowance, AddToAllowance};
};

export default useAllowances;
