
const useDays = () => {
    const date = new Date();
    const dayOfMonth = date.getDate();
    const dayOfMonthPlusOne = new Date(date.getTime() + 86400000).getDate();
    const NumberOfDaysInMonth = ():number => {
        const m = date.getMonth();
        const y = date.getFullYear();
        return new Date(y, m + 1, 0).getDate();
      };

      const DaysFromToday = (array:any[], day:any = dayOfMonth) => array.splice(day);
      const DaysUntilToday = (array:any[], day:any = dayOfMonth) => array.splice(0, day);

  return {dayOfMonth, dayOfMonthPlusOne,  NumberOfDaysInMonth, DaysUntilToday, DaysFromToday}}

export default useDays;