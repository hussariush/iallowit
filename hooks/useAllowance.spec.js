import * as React from 'react';
import renderer from 'react-test-renderer';
import { renderHook, act } from '@testing-library/react-hooks'
import useAllowance from './useAllowance';
import MockDate from 'mockdate'

beforeEach(() => {
    MockDate.set('2021-01-30')
});

afterEach(() => {
  MockDate.reset();
});

const testArray = [
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 1,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 2,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 3,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 4,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 5,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 6,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 7,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 8,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 9,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 10,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 11,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 12,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 13,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 14,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 15,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 16,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 17,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 18,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 19,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 20,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 21,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 22,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 23,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 24,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 25,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 26,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 27,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 28,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 29,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 30,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 31,
    },
  ];

describe('useAllowance hook', () => {
    it(`updates allowance array after deduction`, () => {
        const expectedResult =  [
            { additions: [], allowance: 10, deductions: [], id: 1 },
            { additions: [], allowance: 10, deductions: [], id: 2 },
            { additions: [], allowance: 10, deductions: [], id: 3 },
            { additions: [], allowance: 10, deductions: [], id: 4 },
            { additions: [], allowance: 10, deductions: [], id: 5 },
            { additions: [], allowance: 10, deductions: [], id: 6 },
            { additions: [], allowance: 10, deductions: [], id: 7 },
            { additions: [], allowance: 10, deductions: [], id: 8 },
            { additions: [], allowance: 10, deductions: [], id: 9 },
            { additions: [], allowance: 10, deductions: [], id: 10 },
            { additions: [], allowance: 6, deductions: [ 4 ], id: 11 },
            { additions: [], allowance: 10, deductions: [], id: 12 },
            { additions: [], allowance: 10, deductions: [], id: 13 },
            { additions: [], allowance: 10, deductions: [], id: 14 },
            { additions: [], allowance: 10, deductions: [], id: 15 },
            { additions: [], allowance: 10, deductions: [], id: 16 },
            { additions: [], allowance: 10, deductions: [], id: 17 },
            { additions: [], allowance: 10, deductions: [], id: 18 },
            { additions: [], allowance: 10, deductions: [], id: 19 },
            { additions: [], allowance: 10, deductions: [], id: 20 },
            { additions: [], allowance: 10, deductions: [], id: 21 },
            { additions: [], allowance: 10, deductions: [], id: 22 },
            { additions: [], allowance: 10, deductions: [], id: 23 },
            { additions: [], allowance: 10, deductions: [], id: 24 },
            { additions: [], allowance: 10, deductions: [], id: 25 },
            { additions: [], allowance: 10, deductions: [], id: 26 },
            { additions: [], allowance: 10, deductions: [], id: 27 },
            { additions: [], allowance: 10, deductions: [], id: 28 },
            { additions: [], allowance: 10, deductions: [], id: 29 },
            { additions: [], allowance: 10, deductions: [], id: 30 },
            { additions: [], allowance: 10, deductions: [], id: 31 }
          ];
        const {result} = renderHook(() => useAllowance());
      expect(result.current.DeductFromAllowance(testArray,10,4)).toStrictEqual(expectedResult);
    });
    it(`updates allowance array after addition`, () => {
      const expectedResult =  [
          { additions: [], allowance: 10, deductions: [], id: 1 },
          { additions: [], allowance: 10, deductions: [], id: 2 },
          { additions: [], allowance: 10, deductions: [], id: 3 },
          { additions: [], allowance: 10, deductions: [], id: 4 },
          { additions: [], allowance: 10, deductions: [], id: 5 },
          { additions: [], allowance: 10, deductions: [], id: 6 },
          { additions: [], allowance: 10, deductions: [], id: 7 },
          { additions: [], allowance: 10, deductions: [], id: 8 },
          { additions: [], allowance: 10, deductions: [], id: 9 },
          { additions: [], allowance: 10, deductions: [], id: 10 },
          { additions: [4], allowance: 14, deductions: [], id: 11 },
          { additions: [], allowance: 10, deductions: [], id: 12 },
          { additions: [], allowance: 10, deductions: [], id: 13 },
          { additions: [], allowance: 10, deductions: [], id: 14 },
          { additions: [], allowance: 10, deductions: [], id: 15 },
          { additions: [], allowance: 10, deductions: [], id: 16 },
          { additions: [], allowance: 10, deductions: [], id: 17 },
          { additions: [], allowance: 10, deductions: [], id: 18 },
          { additions: [], allowance: 10, deductions: [], id: 19 },
          { additions: [], allowance: 10, deductions: [], id: 20 },
          { additions: [], allowance: 10, deductions: [], id: 21 },
          { additions: [], allowance: 10, deductions: [], id: 22 },
          { additions: [], allowance: 10, deductions: [], id: 23 },
          { additions: [], allowance: 10, deductions: [], id: 24 },
          { additions: [], allowance: 10, deductions: [], id: 25 },
          { additions: [], allowance: 10, deductions: [], id: 26 },
          { additions: [], allowance: 10, deductions: [], id: 27 },
          { additions: [], allowance: 10, deductions: [], id: 28 },
          { additions: [], allowance: 10, deductions: [], id: 29 },
          { additions: [], allowance: 10, deductions: [], id: 30 },
          { additions: [], allowance: 10, deductions: [], id: 31 }
        ];
      const {result} = renderHook(() => useAllowance());
    expect(result.current.AddToAllowance(testArray,10,4)).toStrictEqual(expectedResult);
  });
    it(`updates allowance array based on day of the month`, () => {
        const expectedResult = [
            { additions: [], allowance: 10, deductions: [], id: 1 },
            { additions: [], allowance: 10, deductions: [], id: 2 },
            { additions: [], allowance: 10, deductions: [], id: 3 },
            { additions: [], allowance: 10, deductions: [], id: 4 },
            { additions: [], allowance: 10, deductions: [], id: 5 },
            { additions: [], allowance: 10, deductions: [], id: 6 },
            { additions: [], allowance: 10, deductions: [], id: 7 },
            { additions: [], allowance: 10, deductions: [], id: 8 },
            { additions: [], allowance: 10, deductions: [], id: 9 },
            { additions: [], allowance: 10, deductions: [], id: 10 },
            { additions: [], allowance: 10, deductions: [], id: 11 },
            { additions: [], allowance: 10, deductions: [], id: 12 },
            { additions: [], allowance: 10, deductions: [], id: 13 },
            { additions: [], allowance: 10, deductions: [], id: 14 },
            { additions: [], allowance: 10, deductions: [], id: 15 },
            { additions: [], allowance: 10, deductions: [], id: 16 },
            { additions: [], allowance: 10, deductions: [], id: 17 },
            { additions: [], allowance: 10, deductions: [], id: 18 },
            { additions: [], allowance: 10, deductions: [], id: 19 },
            { additions: [], allowance: 10, deductions: [], id: 20 },
            { additions: [], allowance: 10, deductions: [], id: 21 },
            { additions: [], allowance: 10, deductions: [], id: 22 },
            { additions: [], allowance: 10, deductions: [], id: 23 },
            { additions: [], allowance: 10, deductions: [], id: 24 },
            { additions: [], allowance: 10, deductions: [], id: 25 },
            { additions: [], allowance: 10, deductions: [], id: 26 },
            { additions: [], allowance: 10, deductions: [], id: 27 },
            { additions: [], allowance: 10, deductions: [], id: 28 },
            { additions: [], allowance: 10, deductions: [], id: 29 },
            { additions: [], allowance: 10, deductions: [], id: 30 },
            { additions: [], allowance: 310, deductions: [], id: 31 }
        ]  
        const {result} = renderHook(() => useAllowance());
      expect(result.current.RecalculateAllowances(testArray,{day:31, update:false})).toStrictEqual(expectedResult);
    });

    it(`updated allowance array after an operation update`, () => {
      const localTest = [
        { additions: [], allowance: 10, deductions: [], id: 1 },
        { additions: [], allowance: 10, deductions: [], id: 2 },
        { additions: [], allowance: 10, deductions: [], id: 3 },
        { additions: [], allowance: 10, deductions: [], id: 4 },
        { additions: [], allowance: 10, deductions: [], id: 5 },
        { additions: [], allowance: 10, deductions: [], id: 6 },
        { additions: [], allowance: 10, deductions: [], id: 7 },
        { additions: [], allowance: 10, deductions: [], id: 8 },
        { additions: [], allowance: 10, deductions: [], id: 9 },
        { additions: [], allowance: 10, deductions: [], id: 10 },
        { additions: [], allowance: 10, deductions: [], id: 11 },
        { additions: [], allowance: 10, deductions: [], id: 12 },
        { additions: [], allowance: 10, deductions: [], id: 13 },
        { additions: [], allowance: 10, deductions: [], id: 14 },
        { additions: [], allowance: 10, deductions: [], id: 15 },
        { additions: [], allowance: 10, deductions: [], id: 16 },
        { additions: [], allowance: 10, deductions: [], id: 17 },
        { additions: [], allowance: 10, deductions: [], id: 18 },
        { additions: [], allowance: 10, deductions: [], id: 19 },
        { additions: [], allowance: 10, deductions: [], id: 20 },
        { additions: [], allowance: 10, deductions: [], id: 21 },
        { additions: [], allowance: 10, deductions: [], id: 22 },
        { additions: [], allowance: 10, deductions: [], id: 23 },
        { additions: [], allowance: 10, deductions: [], id: 24 },
        { additions: [], allowance: 10, deductions: [], id: 25 },
        { additions: [], allowance: 10, deductions: [], id: 26 },
        { additions: [], allowance: 10, deductions: [], id: 27 },
        { additions: [], allowance: 10, deductions: [], id: 28 },
        { additions: [], allowance: 10, deductions: [], id: 29 },
        { additions: [], allowance: 150, deductions: [5], id: 30 },
        { additions: [], allowance: 155, deductions: [], id: 31 }
    ]
      const expectedResult = [        { additions: [], allowance: 10, deductions: [], id: 1 },
      { additions: [], allowance: 10, deductions: [], id: 2 },
      { additions: [], allowance: 10, deductions: [], id: 3 },
      { additions: [], allowance: 10, deductions: [], id: 4 },
      { additions: [], allowance: 10, deductions: [], id: 5 },
      { additions: [], allowance: 10, deductions: [], id: 6 },
      { additions: [], allowance: 10, deductions: [], id: 7 },
      { additions: [], allowance: 10, deductions: [], id: 8 },
      { additions: [], allowance: 10, deductions: [], id: 9 },
      { additions: [], allowance: 10, deductions: [], id: 10 },
      { additions: [], allowance: 10, deductions: [], id: 11 },
      { additions: [], allowance: 10, deductions: [], id: 12 },
      { additions: [], allowance: 10, deductions: [], id: 13 },
      { additions: [], allowance: 10, deductions: [], id: 14 },
      { additions: [], allowance: 10, deductions: [], id: 15 },
      { additions: [], allowance: 10, deductions: [], id: 16 },
      { additions: [], allowance: 10, deductions: [], id: 17 },
      { additions: [], allowance: 10, deductions: [], id: 18 },
      { additions: [], allowance: 10, deductions: [], id: 19 },
      { additions: [], allowance: 10, deductions: [], id: 20 },
      { additions: [], allowance: 10, deductions: [], id: 21 },
      { additions: [], allowance: 10, deductions: [], id: 22 },
      { additions: [], allowance: 10, deductions: [], id: 23 },
      { additions: [], allowance: 10, deductions: [], id: 24 },
      { additions: [], allowance: 10, deductions: [], id: 25 },
      { additions: [], allowance: 10, deductions: [], id: 26 },
      { additions: [], allowance: 10, deductions: [], id: 27 },
      { additions: [], allowance: 10, deductions: [], id: 28 },
      { additions: [], allowance: 10, deductions: [], id: 29 },
      { additions: [], allowance: 152.5, deductions: [ 5 ], id: 30 },
      { additions: [], allowance: 152.5, deductions: [], id: 31 }
    ]
      const {result} = renderHook(() => useAllowance());
      expect(result.current.RecalculateAllowances(localTest,{day:30, update:true})).toStrictEqual(expectedResult);
  });

  // TODO 
  // add tests for faulty data
  // add tests for no data
  // add test for partial data

})
