import * as React from 'react';
import renderer from 'react-test-renderer';
import { renderHook, act } from '@testing-library/react-hooks'
import useDays from './useDays';
import MockDate from 'mockdate'

beforeEach(() => {
    MockDate.set('2021-01-30')
});

afterEach(() => {
  MockDate.reset();
});

const testArray = [
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 1,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 2,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 3,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 4,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 5,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 6,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 7,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 8,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 9,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 10,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 11,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 12,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 13,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 14,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 15,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 16,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 17,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 18,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 19,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 20,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 21,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 22,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 23,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 24,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 25,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 26,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 27,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 28,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 29,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 30,
    },
    {
      additions: [],
      allowance: 10,
      deductions: [],
      id: 31,
    },
  ];

describe('useDays hook', () => {
    it(`gives correct day of month`, () => {
        const expectedResult = 30;
        const {result} = renderHook(() => useDays());
        // act(() => {
        //     result.dayOfMonth
        // })
      expect(result.current.dayOfMonth).toBe(expectedResult);
    });

    it(`gives correct number days of the month`, () => {
        const expectedResult = 31;
        const {result} = renderHook(() => useDays());
        // act(() => {
        //     result.current.NumberOfDaysInMonth()
        // })
      expect(result.current.NumberOfDaysInMonth()).toBe(expectedResult);
    });

    it(`gives array of days up to and including today`, () => {
        const expectedResult = [
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 1,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 2,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 3,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 4,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 5,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 6,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 7,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 8,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 9,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 10,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 11,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 12,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 13,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 14,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 15,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 16,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 17,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 18,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 19,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 20,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 21,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 22,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 23,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 24,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 25,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 26,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 27,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 28,
            },
            {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 29,
            },
            {
                additions: [],
                allowance: 10,
                deductions: [],
                id: 30,
              }
        ];
        const {result} = renderHook(() => useDays());
      expect(result.current.DaysUntilToday(testArray)).toStrictEqual(expectedResult);
    });

    it.skip(`gives array of days from today`, () => {
      const expectedResult = [
        {
              additions: [],
              allowance: 10,
              deductions: [],
              id: 31,
            }
      ];
      const {result} = renderHook(() => useDays());
      expect(result.current.DaysFromToday(testArray,31)).toStrictEqual(expectedResult);
  });

  // TODO 
  // add tests for faulty data
  // add tests for no data
  // add test for partial data
})
