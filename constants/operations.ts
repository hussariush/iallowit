enum Operation {
    subtract = 'SUBTRACT',
    add = 'ADD'
}

export default Operation;