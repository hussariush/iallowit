import React, { useEffect, useState } from 'react';
import { StyleSheet, TextInput, Button } from 'react-native';

import { Text, View } from './Themed';
import { TotalAmountContainer } from '../containers/TotalAmountContainer';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { DayAllowance } from './Listing/Listing';

const EditScreenInfo = () => {
  const [value, onChangeValue] = useState<string>('0');
  const totalAmount = TotalAmountContainer.useContainer();
  const navigation = useNavigation();

  const getAllowancesFromStorage = async () => {
    try {
      const val = await AsyncStorage.getItem('allowances');
      return val  !== null ? JSON.parse(val) : null
    } catch (error) {
      console.error('Something wrong with local storage')
    }
  }

  useEffect(() => {
    getAllowancesFromStorage().then((res:DayAllowance[] | null) => {
      if (res) {
        navigation.navigate('ListingScreen', {screen: 'Listing', params: {fromLocalStorage: true}})
      } 
    })
  },[])

  return (
    <View>      
        <Text
          style={styles.getStartedText}
          lightColor="rgba(0,0,0,0.8)"
          darkColor="rgba(255,255,255,0.8)">
          Enter your amount
        </Text>
        <Text
          style={styles.getStartedText}
          lightColor="rgba(0,0,0,0.8)"
          darkColor="rgba(255,255,255,0.8)">
          {value}
        </Text>
        <TextInput
        style={{ height: 40, borderColor: 'gray', borderWidth: 1, width: 200 }}
        keyboardType={'numeric'}
        onChangeText={amount => onChangeValue(amount)}
        onBlur={() => totalAmount.handleTotalAmount(+value)}
        value={value}
       />
       <Button
        title="Calculate"
        color="#f194ff"
        onPress={ () => navigation.navigate('ListingScreen', {screen: 'Listing', params: {fromLocalStorage: false}})}
      />
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
    flexDirection: 'column',
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    lineHeight: 24,
    textAlign: 'center',
  },
  helpContainer: {
    marginTop: 15,
    marginHorizontal: 20,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    textAlign: 'center',
  },
});

export default EditScreenInfo;