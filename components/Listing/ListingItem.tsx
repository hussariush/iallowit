import React, { useState } from "react";
import { View, Text, Button, TouchableOpacity, TextInput, StyleSheet, Platform, UIManager, LayoutAnimation } from "react-native";
import { Switch } from "react-native-gesture-handler";
import Operation from "../../constants/operations";
import { DayAllowance } from "./Listing";

if (Platform.OS === 'android' && UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
}

const ListingItem = ({ item, onPressHandler, index, isDisabled, isToday }: { item: DayAllowance, onPressHandler: (operation: Operation, val: string, index: number) => void, index: any, isDisabled: boolean, isToday: boolean }) => {
    const [deductionValue, setDeductionValue] = useState<string>('0');
    const [additionValue, setAdditionValue] = useState<string>('0');
    const [isOpen, setIsOpen] = useState<boolean>(false);
    const [isSubtraction, setIsSubtraction] = useState<boolean>(true);
    const toggleSwitch = () => setIsSubtraction(prevState => !prevState)
    const onDeductValue = (value: string) => setDeductionValue(value);
    const onAddValue = (value: string) => setAdditionValue(value);

    const clickHandle = () => {
        if (isSubtraction) {
            onPressHandler(Operation.subtract, deductionValue, index)
        }
        if (!isSubtraction) {
            onPressHandler(Operation.add, additionValue, index)
        }
    }

    return (
        <TouchableOpacity style={isToday ? styles.todayWrapper : styles.wrapper} onPress={() => { LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut); setIsOpen(isOpen => !isOpen) }}>
            <View style={isDisabled && styles.isDisabled}>
                <Text>
                    Date: {item.id}
                </Text>
                <Text>
                    amount for today: {parseFloat(item.allowance as string).toFixed(2)}
                </Text>
                {!!item.deductions.length && <Text>
                    deductions: {item.deductions}
                </Text>}
                {!!item.additions.length && <Text>
                    additions: {item.additions}
                </Text>}
                {!isDisabled && isOpen &&
                    <View>
                        <Text style={!isSubtraction && styles.activeLabel}>
                            I'm adding
                     </Text>
                        <Switch
                            trackColor={{ false: "#767577", true: "#81b0ff" }}
                            thumbColor={"#f5dd4b"}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={toggleSwitch}
                            value={isSubtraction}
                        />
                        <Text style={isSubtraction && styles.activeLabel}>
                            I'm subtracting
                     </Text>
                        {isSubtraction && <View style={{ flex: 1, margin: 5 }}>
                            <TextInput
                                style={styles.input}
                                keyboardType={'numeric'}
                                onChangeText={amount => onDeductValue(amount)}
                                value={deductionValue}
                                placeholder='Deduct this much'
                                placeholderTextColor='#000'
                                editable={!isDisabled}
                            />
                            {/* <Button disabled={isDisabled} title={'deduct'} onPress={() => clickHandle(deductionValue)} /> */}
                        </View>}
                        {!isSubtraction && <View style={{ flex: 1, margin: 5 }}>
                            <TextInput
                                style={styles.input}
                                keyboardType={'numeric'}
                                onChangeText={amount => onAddValue(amount)}
                                value={additionValue}
                                placeholder='Add this much'
                                placeholderTextColor='#000'
                                editable={!isDisabled}
                            />
                        </View>}
                            <Button disabled={isDisabled} title={isSubtraction ? 'subtract' : 'add'} onPress={() => clickHandle()} />
                    </View>
                }</View>
        </TouchableOpacity>

    );
};

const styles = StyleSheet.create({
    isDisabled: {
        backgroundColor: "#666",
        opacity: 0.5
    },
    wrapper: {
        marginBottom: 10,
        backgroundColor: 'goldenrod',
        padding: 5
    },
    todayWrapper: {
        marginBottom: 10,
        backgroundColor: 'lightblue',
        padding: 15
    },
    activeLabel: {
        backgroundColor: 'orange'
    },
    twoCol: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
    input: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        backgroundColor: '#f0f0f0',
        padding: 5,
        marginBottom: 5

    }
})
export default ListingItem;
