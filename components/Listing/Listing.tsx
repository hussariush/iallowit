import * as WebBrowser from "expo-web-browser";
import React, { useEffect } from "react";
import {
  StyleSheet,
  Button,
  SafeAreaView,
} from "react-native";

import Colors from "../../constants/Colors";
import { Text, View } from "../Themed";
import { TotalAmountContainer } from "../../containers/TotalAmountContainer";
import {
  FlatList,
} from "react-native-gesture-handler";
import ListingItem from "./ListingItem";
import useDays from "../../hooks/useDays";
import { DailyAllowancesContainer } from "../../containers/DailyAllowancesContainer";
import { useNavigation } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import useAllowances from "../../hooks/useAllowance";
import Operation from "../../constants/operations";

export type DayAllowance = {
  id: number;
  allowance: number | string;
  deductions: any[];
  additions: any[];
};

const Listing = ({ routeParams: { params } }) => {
  const { totalAmount } = TotalAmountContainer.useContainer();
  const { dailyAllowances, SaveAllowances } = DailyAllowancesContainer.useContainer();
  const { dayOfMonth, dayOfMonthPlusOne, NumberOfDaysInMonth } = useDays();
  const navigation = useNavigation();
  const { RecalculateAllowances, DeductFromAllowance, AddToAllowance } = useAllowances();
  const allowanceDays = [...new Array(NumberOfDaysInMonth())].map(
    (val: any, index: number): DayAllowance => {
      return {
        id: index + 1,
        allowance: totalAmount / NumberOfDaysInMonth(),
        deductions: [],
        additions: [],
      };
    }
  );

  const clearAll = async () => {
    try {
      await AsyncStorage.clear()
    } catch (e) {
      console.debug('Clearing error')
    }
    navigation.navigate('Root')
  }

  const getAllowancesFromStorage = async () => {
    try {
      const val = await AsyncStorage.getItem('allowances');
      return val !== null ? JSON.parse(val) : null
    } catch (error) {
      console.error('Something wrong with local storage')
    }
  }

  const setAllowancesToStorage = async (val) => {
    try {
      const jsonValue = JSON.stringify(val)
      await AsyncStorage.setItem('allowances', jsonValue)
    } catch (e) {
      console.error('Something wrong with saving to local storage')
    }
  }

  useEffect(() => {
    if (params.fromLocalStorage) {
      getAllowancesFromStorage().then((res) => {
        const updatedData = RecalculateAllowances(res);
        SaveAllowances(updatedData);
        setAllowancesToStorage(updatedData)
      })
    } else {
      const updatedData = RecalculateAllowances(allowanceDays);
      SaveAllowances(updatedData);
      setAllowancesToStorage(updatedData).then(res => console.debug(res))
    }

  }, [totalAmount])

  const operationHandler = (operation: Operation, value: string, index: any) => {
    let newAllowances: any = null;
    if (operation === Operation.add) {
      newAllowances = AddToAllowance(dailyAllowances, index, value);
    } else {
      newAllowances = DeductFromAllowance(dailyAllowances, index, value);
    }
    const updatedData = RecalculateAllowances(newAllowances, { day: dayOfMonth, update: true });
    updatedData && SaveAllowances(updatedData)
    setAllowancesToStorage(updatedData)
  }

  const renderItem = ({ item, index }: { item: any, index: any }) => {
    return (
      <ListingItem
        item={item}
        index={index}
        onPressHandler={operationHandler}
        isDisabled={dayOfMonth > item.id}
        isToday={dayOfMonth === item.id}
      />
    );
  };


  return (
    <SafeAreaView style={styles.container}>
      <Text
        style={styles.getStartedText}
        lightColor="rgba(0,0,0,0.8)"
        darkColor="rgba(255,255,255,0.8)"
      > Your daily allowances
      </Text>
      <FlatList
        style={styles.scrollElem}
        data={dailyAllowances}
        keyExtractor={(item, index) => `${index}`}
        renderItem={renderItem}
        extraData={totalAmount}
      />
      <View style={styles.buttonContainer}>
        <View>
          <Button
            title="Back"
            color="#f194ff"
            onPress={() => navigation.goBack()}
          />

        </View>
        <View>
          <Button
            title="Reset"
            color="#f194ff"
            onPress={clearAll}
          />

        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'stretch'
  },
  getStartedText: {
    fontSize: 17,
    lineHeight: 24,
    textAlign: "center",
  },
  scrollElem: {
    flex: 1
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    padding: 5
  }
});

export default Listing;
